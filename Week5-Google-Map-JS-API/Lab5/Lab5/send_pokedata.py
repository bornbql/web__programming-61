import requests
import time
import random

url = 'http://iot.data.ubu.ac.th/api/save'
thingName = 'pokemonDemo' # choose your thing's name

while True:
    pokemons = [{'lat': 15.1258068,'lng': 104.9073150,'name': "Pikachu"},
                {'lat': 15.1236668,'lng': 104.9015333,'name': "Kabigon"},
                {'lat': 15.1226700,'lng': 104.9103210,'name': "Gongon"}]

    for pokeData in pokemons:
        x = random.uniform(0.0001, 0.003)
        y = random.uniform(0.0001, 0.003)
        if random.choice(['+', '-'])=='-':
            x = -x
        if random.choice(['+', '-'])=='-':
            y = -y
        pokeData['lat'] += x
        pokeData['lng'] += y

    print(pokemons)
    
    for pokeData in pokemons:   
        data = {'name': thingName, 'content': pokeData}
        r = requests.post(url, json=data)
        print('Status:', r.status_code)
        time.sleep(10)
