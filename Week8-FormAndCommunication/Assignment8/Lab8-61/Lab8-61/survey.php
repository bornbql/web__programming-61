<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>UBU Survey</title>
  </head>
  <body>
    <form action="#">
      <p>No. A4567</p>
      <p>Nickname: <input type="text" id="nickname" name=""></p>
      <p>วันเกิด <input type="text"> เดือน <input type="text"> พ.ศ. <input type="text"></p>
      <fieldset id="field1">
          <legend>อาหารที่ชอบ</legend>
          <p><label><input type="checkbox"/> ก๋วยเตี๋ยว</label></p>
          <p><label><input type="checkbox"/> ส้มตำ</label></p>
          <p><label><input type="checkbox"/> ไก่ย่าง</label></p>
          <p><label><input type="checkbox"/> ลาบหมู</label></p>
      </fieldset>
    </form>
    <form action="#">
      <fieldset id="field2">
          <legend>สีที่ชอบ</legend>
          <p><label><input type="checkbox"/> ขาว</label></p>
          <p><label><input type="checkbox"/> ดำ</label></p>
          <p><label><input type="checkbox"/> ส้ม</label></p>
          <p><label><input type="checkbox"/> ฟ้า</label></p>
          <p><label><input type="checkbox"/> แดง</label></p>
      </fieldset>
    </form>

    <script>
      // Write your JavaScript here..
    </script>
  </body>
</html>
