<style>
  table, th, td{
    border: 1px solid black;
    border-collapse: collapse;
    text-align: center;
  }
</style>

<?php
function html_table($data = array())
{
    $rows = array();
    foreach ($data as $row) {
        $cells = array();
        foreach ($row as $cell) {
            $cells[] = "<td>{$cell}</td>";
        }
        $rows[] = "<tr>" . implode('', $cells) . "</tr>";
    }
    return "<table class='hci-table'>" . implode('', $rows) . "</table>";
}
$data = array(
    array('ผลไม้' => 'ผลไม้', 'ราคา (ต่อหน่วย)' => 'ราคา (ต่อหน่วย)'),
    array('ผลไม้' => 'apple', 'ราคา (ต่อหน่วย)' => '40'),
    array('ผลไม้' => 'cherry', 'ราคา (ต่อหน่วย)' => '27'),
    array('ผลไม้' => 'banana', 'ราคา (ต่อหน่วย)' => '35'),
    array('ผลไม้' => 'mango', 'ราคา (ต่อหน่วย)' => '43'),
    array('ผลไม้' => 'pineapple', 'ราคา (ต่อหน่วย)' => '30'),
    array('ผลไม้' => 'grape', 'ราคา (ต่อหน่วย)' => '89'),
    array('ผลไม้' => 'melon', 'ราคา (ต่อหน่วย)' => '88'),
    array('ผลไม้' => 'orange', 'ราคา (ต่อหน่วย)' => '20'),
    );
echo html_table($data);
