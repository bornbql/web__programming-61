<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
    Day
    <select name="days">
      <?php
        for ($day=1; $day <=31 ; $day++) {
          echo "<option>".$day."</option>";
        }
       ?>
    </select>
    Month
    <select name="months">
      <?php
        for ($mon=1; $mon <=12 ; $mon++) {
          echo "<option>".$mon."</option>";
        }
       ?>
    </select>
    Years
    <select name="years">
      <?php
        for ($year=1980; $year <=2020 ; $year++) {
          echo "<option>".$year."</option>";
        }
       ?>
    </select>

  </body>
</html>